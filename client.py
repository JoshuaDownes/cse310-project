import sys
from socket import *

# Method to authenticate connection with 3 way handshake
def start_handshake(clientSocket):
        clientSocket.send('CONN \r\n')
        shake = clientSocket.recv(1024)
        print shake
        if shake == 'NNOC \r\n':
            clientSocket.send('HELLO \r\n')
            return True
        else:
            print 'Error connecting to server. Exitting...'
            sys.exit()

def get_type_server(clientSocket, serverName):
    while(1):
        requestedType = raw_input('Enter record type requested: ')
        clientSocket.send('TYPE ' + requestedType + ' \r\n')
        server_port = clientSocket.recv(1024)
        words = server_port.split(" ")
        # Check we have given a valid type
        if words[0] == 'EPYT':
            # Connect to name server
            clientSocket.close()
            clientSocket = socket(AF_INET, SOCK_STREAM)
            clientSocket.connect((serverName, int(words[1])))
            return clientSocket
        else:
            print "Error, no such type found"

# Main
if __name__ == '__main__':
    # Check that we have 2 arguments
    if(len(sys.argv) != 3):
        print "Invalid args. \r\nUSAGE: python client.py IP_ADDRESS PORT"
        sys.exit()
    # Grabs args
    serverName = sys.argv[1] 
    serverPort = int(sys.argv[2])
    # Creates connection with server
    clientSocket = socket(AF_INET,SOCK_STREAM)
    clientSocket.connect((serverName,serverPort))
    start_handshake(clientSocket)
    # Get nameserver type from user
    clientSocket = get_type_server(clientSocket, serverName)
    # Loops around (add protocol here)
    while 1:
        sentence = raw_input('client> ')
        words = sentence.split(" ")
        verb = words[0]
        if verb.lower() == 'help':
            print 'The following commands are supported: \nput NAME VALUE TYPE\nget NAME TYPE\ndel NAME TYPE\nbrowse\nexit'
        elif verb.lower() == 'type':
            clientSocket.send(sentence)
            response = clientSocket.recv(1024)
            print response
        elif verb.lower() == 'put':
            #check proper formatting of input
                        if (len(words) != 3):
                            print 'Incorrect formatting. Try Again.'
                        else:
                            #formatting ok. enter into file
                                send_format = 'PUT ' + str(words[1]) + ' ' + str(words[2]) + ' \r\n'
                                clientSocket.send(send_format)
                                #get server response
                                response = clientSocket.recv(1024)
                                check = response.split(" ")
                                if check[0] == 'ERR':
                                    print 'Entry could not be added\n'
                                else:
                                    print 'Successful insertion of entry\n'
        elif verb.lower() == 'get':
            #check proper formatting of input
                        if (len(input) != 3):
                            print 'Incorrect formatting. Try Again.'
                        else:
                            #formatting ok. enter into file
                                clientSocket.send(sentence)
                                #server response
                                response = clientSocket.recv(1024)
                                #check to see if failed of success
                                check = response.split(" ")
                                if check[0] == 'failed':
                                    print 'Does not exist\n'
                                else:
                                    print 'got entry: ' + response + '\n'
        elif verb.lower() == 'del':
            #check proper formatting of input
                        if (len(input) != 3):
                            print 'Incorrect formatting. Try Again.'
                        else:
                            #formatting ok. enter into file
                                clientSocket.send(sentence)
                                #get server response
                                response = clientSocket.recv(1024)
                                #check to see if failed of success
                                check = response.split(" ")
                                if check[0] == 'failed':
                                    print 'Entry does not exist\n'
                                else:
                                    print 'deleted entry: ' + sentence + '\n'
        elif verb.lower() == 'browse':
            clientSocket.send('browse')
            #get server response
            response = clientSocket.recv(48024)
            check = response.split(" ")
            #if check[0] == 'failed':
            print response
        elif verb.lower() == 'exit':
            #close connection and exit
            clientSocket.send("EXIT \r\n")
            clientSocket.close()
            exit()
        elif verb.lower() == 'done':
            # Creates connection back with manager
            clinetSocket.send("DONE \r\n")
            clientSocket.close()
            clientSocket = socket(AF_INET,SOCK_STREAM)
            clientSocket.connect((serverName,serverPort))
        else:
            print 'command ' + verb + ' not recognized. Type help for usage information.\n'
