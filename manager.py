# Imports
import time
import json
import os
import sys
from socket import *
from threading import *
from os import pipe
from os import fork
from os import execv

# Class to handle each client thread
class ClientThread(Thread):
    # Consructor for new client thread
    def __init__(self, connectionSock, address, types, lock):
        # Start thread with self as parameter
        Thread.__init__(self)
        # Initialize all parameters
        self.connectionSocket = connectionSock
        self.addr = address
        self.types = types
        self.lock = lock
        self.notClosed = True
    # Run method for thread
    def run(self):
        try:
            self.start_handshake(self.connectionSocket)
            while(self.notClosed):
                # Receive message
                message = self.connectionSocket.recv(1024)
                # Respond to message appropriately
                self.respond(message);
            self.connectionSocket.close()
        except IOError:
            # Send response message for file not found
            err = "Error establishing socket.\r\n\r\n"
            self.connectionSocket.send(err)
            # Close client socket
            self.connectionSocket.close() 
    # Method for authentication
    def start_handshake(self, connectionSocket):
        first_shake = connectionSocket.recv(1024)
        if first_shake == 'CONN \r\n':
            connectionSocket.send('NNOC \r\n')
            connectionSocket.recv(1024)
        else:
            connectionSocket.send('ERR 00 Unable to authenticate')
            notClosed = False
    # Method for responding to client messages
    def respond(self, message):
        # Split message words into list
        words = message.split(' ');
        print "DEBUG: ", words
        verb = words[0];
        print "RECEIVED: ", verb
        if verb == 'EXIT':
            print "User exit..."
            #self.connectionSocket.send('TIXE')
            self.connectionSocket.close()
            self.notClosed = False
        elif verb == 'TYPE':
            lock.acquire()
            ret = "ERR 03 Type not found \r\n"
            for key in self.types:
		# parse out the endline characters in manager.in
                str1 = key.split(" ")
                if str1[0] == words[1]:
                    ret = "EPYT " + str(self.types[key]) + " \r\n"
                    self.connectionSocket.send(ret)
                    self.connectionSocket.close()
                    self.notClosed = False
            if self.notClosed == True:
                self.connectionSocket.send(ret)
            lock.release()
        else:
            print message, "Not supported..."
            self.connectionSocket.send('ERR 01 Command not found')
   
# Function to start our nameservers
def begin_servers():
    # Create our pipes
    rend, wend = pipe()
    # Open our manager.ins
    manager_in = open("manager.in", 'r')
    server_types = manager_in.readlines()
    types = {} 
    # Count the lines
    for i in server_types:
        new_server = fork()
        if new_server == 0:
            # Load args
            sys.argv = ["server.py", i[:-1], wend]
            # start server 
            execfile("server.py") 
            exit()
        rec = os.read(rend, 64)
        types[i[:-1]] = rec
    return types

  # Main
if __name__ == '__main__':
    managerSocket = socket(AF_INET, SOCK_STREAM)
    # Bind server socket to group port 
    managerSocket.bind(('',6701))
    # Print port number
    print "Port used for manager: ",managerSocket.getsockname()[1] 
    # Listen for connections 
    managerSocket.listen(1)
    # Create processes for our servers
    types = begin_servers()
    # Keep track of all threads active
    threads = []
    # Create a lock for file editing
    lock = Lock()
    while True:
        # Establish the connection
        print 'Manager active...\n'
        # Assigns client a separate port 
        connectionSocket, addr = managerSocket.accept() 
        print("Client connected to manager at {} \r\n".format(addr))
        # Create client thread
        clientThread = ClientThread(connectionSocket, addr, types, lock)
        # Set daemon so program exits when main thread is over
        clientThread.setDaemon(True)
        # Start client thread
        clientThread.start()
        # Add thread to list
        threads.append(clientThread)
    # Close server socket
    managerSocket.close()

