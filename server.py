# Imports
import time
import json
import os.path
import sys
from socket import *
from threading import *


# Class to handle each client thread
class ClientThread(Thread):
    # Consructor for new client thread
    def __init__(self, connectionSock, address, data_file_name, server_type, lock):
        # Start thread with self as parameter
        Thread.__init__(self)
        # Initialize parameter
        self.connectionSocket = connectionSock
        self.addr = address
        self.data_file_name = data_file_name
        self.server_type = server_type
        self.lock = lock
        # For closing connectoin
        self.notClosed = True
    # Run method for thread
    def run(self):
        try:
            while(self.notClosed):
                # Receive message
                message = self.connectionSocket.recv(1024)
                # Respond to message appropriately
                self.respond(message);
        except IOError:
            # Send response message for file not found
            err = "Error establishing socket.\r\n\r\n"
            self.connectionSocket.send(err)
            # Close client socket
            self.connectionSocket.close() 
    # Method for responding to client messages
    def respond(self, message):
        # Check that our file exists in the directory
        self.check_exists(self.data_file_name);
        # Split message words into list
        words = message.split(' ');
        print "DEBUG: ", words
        verb = words[0];
        print "RECEIVED: ", verb
        if verb == 'EXIT':
            print "User exit..."
            self.connectionSocket.send('tixe')
            self.connectionSocket.close()
            self.notClosed = False
        elif verb == 'PUT':
            self.lock.acquire()
            self.respond_put(words)
            self.lock.release()
        elif verb == 'GET':
            self.lock.acquire()
            self.respond_get(words)
            self.lock.release()
        elif verb == 'DEL':
            self.lock.acquire()
            self.respond_delete(words)
            self.lock.release()
        elif verb == 'BROWSE':
            self.lock.acquire()
            self.respond_browse(words)
            self.lock.release()
        else:
            print message, "Not supported..."
            self.connectionSocket.send('ERR 01 Command not found')
    # Function which responds to "put" requests
    def check_exists(self,data_file_name):
        if not os.path.exists(self.data_file_name):
            data = open(self.data_file_name, 'w')
            data.write('{"entries":[]}')
            data.close()
    def respond_put(self,words):
        # Open json file 
            with open(self.data_file_name, 'r+') as f:
                # Boolean to check if we need to add or just append
                not_found = True;
                data = json.load(f)
                # Check if our name and type already exist in our entries
                for i in range(0,len(data['entries'])):
                    if data['entries'][i]['name'] == words[1]:
                        # Append the value
                        data['entries'][i]['value'] = words[2]
                        not_found = False;
                        f.seek(0)
                        break
                # If not, append it
                if(not_found):
                    data['entries'].append({"name":words[1],"value":words[2],"type":self.server_type})
                    f.seek(0)
                f.truncate()
                json.dump(data, f, indent=4)
                f.close()
            self.connectionSocket.send('tup \r\n')
    # Function which responds to "get" requests
    def respond_get(self,words):
        # Open json file 
            #time.sleep(30)
            with open(self.data_file_name, 'r') as f:
                # Value for returning
                ret = 'ERR 2 Entry not found'
                data = json.load(f)
                # Check for name and type 
                for i in range(0,len(data['entries'])):
                    if data['entries'][i]['name'] == words[1]: #and data['entries'][i]['type'] == words[2]:
                        ret = 'TEG ' + data['entries'][i]['value'] 
                        f.seek(0)
                        break
                f.close()
            ret += ' \r\n'
            self.connectionSocket.send(ret)
    def respond_delete(self,words):
        #Open json file
            with open(self.data_file_name, 'r+') as f:
                # Value for returning
                ret = 'ERR 2 Entry not found \r\n'
                data = json.load(f)
                # Check for name and type 
                for i in range(0,len(data['entries'])):
                    if data['entries'][i]['name'] == words[1]: #and data['entries'][i]['type'] == words[2]:
                        # get rid of the value 
                        data['entries'].pop(i)
                        f.seek(0)
                        ret = 'LED \r\n'
                        break
                f.truncate()
                json.dump(data, f, indent=4)
                f.close()
            self.connectionSocket.send(ret)
    def respond_browse(self,words):
        #Open json file
            ret = "ESWORB "
            ret += self.server_type
            with open(self.data_file_name, 'r') as f:
                data = json.load(f)
                # go through all entries 
                for i in range(0,len(data['entries'])):
                    add_entry = data['entries'][i]['name'] + ' ' + data['entries'][i]['value'] + ' ' # + data['entries'][i]['type'] + '\n'
                    ret += add_entry
                f.close()
            ret += ' \r\n'
            self.connectionSocket.send(ret)


# Main
if __name__ == '__main__':
    # Check for args
    server_type = sys.argv[1]
    wend = sys.argv[2]
    serverSocket = socket(AF_INET, SOCK_STREAM)
    # Bind server socket to ephemereal port (CHANGE TO 0 LATER)
    serverSocket.bind(('127.0.0.1',0))
    # send port to write end of pipe 
    os.write(int(wend), str(serverSocket.getsockname()[1]))
    # Listen for connections 
    serverSocket.listen(1)
    # Keep track of all threads active
    threads = []
    # Create file variable based on name 
    data_file_name = server_type + '_Data.json'
    print data_file_name
    # Create a lock for file editing
    lock = Lock()
    while True:
        # Establish the connection
        print 'Ready to serve...\n'
        # Assigns client a separate port 
        connectionSocket, addr = serverSocket.accept() 
        print addr
        # Create client thread
        clientThread = ClientThread(connectionSocket, addr, data_file_name, server_type, lock)
        # Set daemon so program exits when main thread is over
        clientThread.setDaemon(True)
        # Start client thread
        clientThread.start()
        # Add thread to list
        threads.append(clientThread)
    # Close server socket
    serverSocket.close()



